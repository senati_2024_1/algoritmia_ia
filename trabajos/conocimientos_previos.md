### Conocimientos previos

*  En un documento investigar los siguientes conceptos:
* 1. Datos y cuales son los tipos de datos en Java.
* 2. Definir el concepto de información y plantear unos ejemplos aplicados en la inteligencia artificial.
* 3. Investigar y describir con vuestras propias paralabras el concepto de inteligecia artificial.
* 4. Decribir que es el aprendizaje automático y describir unos ejemplos que podemos aplicar en nuestra realidad puneña.
* 5. Describir que es el aprendizaje supervisado y detallar los algoritmos de esta categoría con un ejemplo cada uno.
* 6. Describir que es el aprendizaje no supervisado y detallar los algoritmos de esta categoríacon un ejemplo cada uno.
* 7. Describir que es un algoritmos de clasificación y detallar los algoritmos de esta categoríacon un ejemplo cada uno.
* 8. Describir que son los algoritmos de regresión y detallar los algoritmos de esta categoríacon un ejemplo cada uno.