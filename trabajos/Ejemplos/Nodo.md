# Implementacion de la clase Nodo.java

```java:
public class Nodo {
    private int datos;
    private Nodo izquierda;
    private Nodo derecha;

    public Nodo(int datos) {
        this.datos = datos;
        this.izquierda = null;
        this.derecha = null;
    }

    public void setLeft(Nodo nodo) {
        if (izquierda == null) {
            izquierda = nodo;
        }
    }

    public void setRight(Nodo nodo) {
        if (derecha == null) {
            derecha = nodo;
        }
    }

    public Nodo getLeft() {
        return izquierda;
    }

    public Nodo getRight() {
        return derecha;
    }

    public int getData() {
        return datos;
    }

    public void setData(int datos) {
        this.datos = datos;
    }

    public static void main(String[] args) {
        // Creación de nodos de ejemplo y prueba de métodos
        Nodo nodo1 = new Nodo(5);
        Nodo nodo2 = new Nodo(10);
        Nodo nodo3 = new Nodo(15);

        nodo1.setLeft(nodo2);
        nodo1.setRight(nodo3);

        System.out.println("Datos del nodo 1: " + nodo1.getData());
        System.out.println("Datos del nodo izquierdo del nodo 1: " + nodo1.getLeft().getData());
        System.out.println("Datos del nodo derecho del nodo 1: " + nodo1.getRight().getData());
    }
}
```