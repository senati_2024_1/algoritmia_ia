# Ejemplo de arbol binario

## Crear las clases y médodos en un paquete ArbolBinario

### Nodo.java

```java:
package ArbolBinario;

public class Nodo {
    private int dato;
    private Nodo izquierda, derecha;

    public Nodo(int dato) {
        this.dato = dato;
        this.izquierda = null;
        this.derecha = null;
    }

    public int getDato() {
        return dato;
    }

    public Nodo getIzquierda() {
        return izquierda;
    }

    public void setIzquierda(Nodo izquierda) {
        this.izquierda = izquierda;
    }

    public Nodo getDerecha() {
        return derecha;
    }

    public void setDerecha(Nodo derecha) {
        this.derecha = derecha;
    }

    public void imprimirDato() {
        System.out.println(this.getDato());
    }
}
```

### Arbol.java

```java:
package ArbolBinario;

public class Arbol {
    private Nodo raiz;

    public Arbol() {
        this.raiz = null;
    }

    public boolean existe(int busqueda) {
        Nodo actual = raiz;
        while (actual != null) {
            if (busqueda == actual.getDato())
                return true;
            else if (busqueda < actual.getDato())
                actual = actual.getIzquierda();
            else
                actual = actual.getDerecha();
        }
        return false;
    }

    public void insertar(int dato) {
        if (raiz == null)
            raiz = new Nodo(dato);
        else {
            Nodo actual = raiz;
            Nodo padre;
            while (true) {
                padre = actual;
                if (dato < actual.getDato()) {
                    actual = actual.getIzquierda();
                    if (actual == null) {
                        padre.setIzquierda(new Nodo(dato));
                        return;
                    }
                } else {
                    actual = actual.getDerecha();
                    if (actual == null) {
                        padre.setDerecha(new Nodo(dato));
                        return;
                    }
                }
            }
        }
    }

    private void preorden(Nodo n) {
        if (n != null) {
            n.imprimirDato();
            preorden(n.getIzquierda());
            preorden(n.getDerecha());
        }
    }

    private void inorden(Nodo n) {
        if (n != null) {
            inorden(n.getIzquierda());
            n.imprimirDato();
            inorden(n.getDerecha());
        }
    }

    private void postorden(Nodo n) {
        if (n != null) {
            postorden(n.getIzquierda());
            postorden(n.getDerecha());
            n.imprimirDato();
        }
    }

    public void preorden() {
        this.preorden(this.raiz);
    }

    public void inorden() {
        this.inorden(this.raiz);
    }

    public void postorden() {
        this.postorden(this.raiz);
    }
}
```

### Main.java

```java:
package ArbolBinario;

public class Main {
    public static void main(String[] argumentos) {
        Arbol arbol = new Arbol();
        arbol.insertar(1);
        arbol.insertar(2);
        arbol.insertar(3);
        arbol.insertar(4);
        arbol.insertar(0);
        System.out.println("Recorriendo inorden:");
        arbol.inorden();
        System.out.println("Recorriendo postorden:");
        arbol.postorden();
        System.out.println("Recorriendo preorden:");
        arbol.preorden();
        System.out.println(arbol.existe(1)); // true
        System.out.println(arbol.existe(7)); // false
        System.out.println(arbol.existe(0)); // true
    }
}
```