# Recorrido de árbol posterior al orden: iterativo y recursivo

Dado un árbol binario, escriba una solución iterativa y recursivo para recorrer el árbol mediante el recorrido posorden en Java.

A diferencia de las listas enlazadas, los arrays unidimensionales y otras estructuras de datos lineales, que se recorren en orden lineal, los árboles se pueden recorrer de múltiples maneras en profundidad de primer orden (hacer un pedido, en orden, y orden de publicación) o ancho de primer orden (recorrido de orden de nivel). Más allá de estos recorridos básicos, son posibles varios esquemas más complejos o híbridos, como búsquedas limitadas en profundidad como la búsqueda iterativa de profundización primero en profundidad. En esta publicación, se analiza en detalle el recorrido del árbol posterior al pedido. 

 Atravesar un árbol implica iterar sobre todos los nodos de alguna manera. Como el árbol no es una estructura de datos lineal, puede haber más de un nodo siguiente posible a partir de un nodo dado, por lo que algunos nodos deben ser diferidos, es decir, almacenados de alguna manera para visitarlos más tarde. El recorrido se puede hacer iterativamente donde los nodos diferidos se almacenan en el stack, o se puede hacer por recursión, donde los nodos diferidos se almacenan implícitamente en el pila de llamadas.

Para atravesar un árbol binario (no vacío) en orden posterior, debemos hacer estas tres cosas para cada nodo n a partir de la raíz del árbol:

![4](/ArbolesBinarios/4.png)

(L) Atraviesa recursivamentemente su subárbol izquierdo. Cuando termine este paso, estamos de vuelta en n otra vez.
(R) Atraviesa recursivamentemente su subárbol derecho. Cuando termine este paso, estamos de vuelta en n otra vez.
(N) Proceso n sí mismo.
 En el recorrido normal posterior al orden, visite el subárbol izquierdo antes que el subárbol derecho. Si visitamos el subárbol derecho antes de visitar el subárbol izquierdo, se denomina recorrido inverso en posorden.

## Implementación recursivo

Como podemos ver, antes de procesar cualquier nodo, primero se procesa el subárbol izquierdo, seguido del subárbol derecho, y finalmente se procesa el nodo. Estas operaciones se pueden definir recursivamentemente para cada nodo. La implementación recursivamente se conoce como Búsqueda en profundidad (DFS), ya que el árbol de búsqueda se profundiza lo más posible en cada hijo antes de pasar al siguiente hermano.

El siguiente es el programa Java que lo demuestre:

```java:
// Estructura de datos para almacenar un nodo de árbol binario
class Node
{
    int data;
    Node left, right;
 
    // Función para crear un nuevo nodo de árbol binario con una clave dada
    public Node(int key)
    {
        data = key;
        left = right = null;
    }
}
 
class Main
{
    // Función recursivo para realizar un recorrido posterior al orden en el árbol
    public static void postorder(Node root)
    {
        // regresa si el nodo actual está vacío
        if (root == null) {
            return;
        }
    
        // Recorrer el subárbol izquierdo
        postorder(root.left);
    
        // Recorrer el subárbol derecho
        postorder(root.right);
    
        // Mostrar la parte de datos de la raíz (o nodo actual)
        System.out.print(root.data + " ");
    }
 
    public static void main(String[] args)
    {
        /* Construimos el siguiente árbol
                   1
                 /   \
                /     \
               2       3
              /      /   \
             /      /     \
            4      5       6
                  / \
                 /   \
                7     8
        */
 
        Node root = new Node(1);
        root.left = new Node(2);
        root.right = new Node(3);
        root.left.left = new Node(4);
        root.right.left = new Node(5);
        root.right.right = new Node(6);
        root.right.left.left = new Node(7);
        root.right.left.right = new Node(8);
 
        postorder(root);
    }
}
```

## Implementación iterativa

Para convertir el procedimiento recursivo anterior en uno iterativo, necesitamos una stack explícita. El siguiente es un algoritmo iterativo simple basado en la stack para realizar el recorrido posterior al pedido:

```sh
iterativePostorder(node)

s —> empty stackt 
t —> output stack

while (not s.isEmpty())
node —> s.pop()
t.push(node)

if (node.left <> null)
    s.push(node.left)

if (node.right <> null)
    s.push(node.right)

while (not t.isEmpty())
    node —> t.pop()
    visit(node)
```

El algoritmo se puede implementar de la siguiente manera en Java:

```java:
import java.util.Stack;
 
// Estructura de datos para almacenar un nodo de árbol binario
class Node
{
    int data;
    Node left, right;
 
    // Función para crear un nuevo nodo de árbol binario con una clave dada
    public Node(int key)
    {
        data = key;
        left = right = null;
    }
}
 
class Main
{
    // Función iterativa para realizar un recorrido posterior al orden en el árbol
    public static void postorderIterative(Node root)
    {
        // regresa si el árbol está vacío
        if (root == null) {
            return;
        }
 
        // crea una stack vacía y empuja el nodo raíz
        Stack<Node> stack = new Stack<>();
        stack.push(root);
    
        // crea otra stack para almacenar el recorrido posterior al pedido
        Stack<Integer> out = new Stack<>();
    
        // bucle hasta que la stack esté vacía
        while (!stack.empty())
        {
            // saca un nodo de la stack y envía los datos a la stack de salida
            Node curr = stack.pop();
            out.push(curr.data);
    
            // empuja el hijo izquierdo y derecho del nodo extraído a la stack
            if (curr.left != null) {
                stack.push(curr.left);
            }
    
            if (curr.right != null) {
                stack.push(curr.right);
            }
        }
    
        // imprime el recorrido posterior al pedido
        while (!out.empty()) {
            System.out.print(out.pop() + " ");
        }
    }
 
    public static void main(String[] args)
    {
        /* Construimos el siguiente árbol
                   1
                 /   \
                /     \
               2       3
              /      /   \
             /      /     \
            4      5       6
                  / \
                 /   \
                7     8
        */
 
        Node root = new Node(1);
        root.left = new Node(2);
        root.right = new Node(3);
        root.left.left = new Node(4);
        root.right.left = new Node(5);
        root.right.right = new Node(6);
        root.right.left.left = new Node(7);
        root.right.left.right = new Node(8);
 
        postorderIterative(root);
    }
}
```

La complejidad temporal de las soluciones anteriores es O(n), dónde n es el número total de nodos en el árbol binario. La complejidad espacial del programa es O(n) ya que el espacio requerido es proporcional a la altura del árbol, que puede ser igual al número total de nodos en el árbol en el peor de los casos para árboles sesgados.

```sh:
Tarea:
Ejercicio: Realice un recorrido iterativo posterior al pedido utilizando solo una stack.
```
