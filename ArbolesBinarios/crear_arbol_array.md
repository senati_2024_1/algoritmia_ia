# Construya un árbol binario a partir de una array principal

Dada una array de enteros que representa un árbol binario, tal que la relación padre-hijo está definida por (A[i], i) para cada índice i en array A, construye un árbol binario a partir de él. El valor del nodo raíz es i si -1 está presente en el índice i en la array. Se puede suponer que la entrada proporcionada al programa es válida.

Por ejemplo,

```java:
Parent: [-1, 0, 0, 1, 2, 2, 4, 4]
Index : [ 0, 1, 2, 3, 4, 5, 6, 7]
```

Tenga en cuenta que,

* -1 está presente en el índice 0, lo que implica que la raíz del árbol binario es el nodo 0.
* 0 está presente en el índice 1 y 2, lo que implica que los hijos izquierdo y derecho del nodo 0 son 1 y 2.
* 1 está presente en el índice 3, lo que implica que el hijo izquierdo o derecho del nodo 1 es 3.
* 2 está presente en el índice 4 y 5, lo que implica que los hijos izquierdo y derecho del nodo 2 son 4 y 5.
* 4 está presente en el índice 6 y 7, lo que implica que los hijos izquierdo y derecho del nodo 4 son 6 y 7.

El árbol binario correspondiente es:

![2](/ArbolesBinarios/2.png)

La solución es simple y efectiva: crear n nuevos nodos de árbol, cada uno con valores de 0 a n-1, dónde n es el tamaño de la array y almacenarlos en un mapa o array para una búsqueda rápida. Luego, recorra la array principal dada y construya el árbol estableciendo la relación padre-hijo definida por (A[i], i) para cada índice i en array A. Dado que se pueden formar varios árboles binarios a partir de una sola entrada, la solución debe construir cualquiera de ellos. La solución siempre establecerá el hijo izquierdo para un nodo antes de establecer su hijo derecho.

El algoritmo se puede implementar de la siguiente Java:

```java:
import java.util.HashMap;
import java.util.Map;
 
// Una clase para almacenar un nodo de árbol binario
class Node
{
    int data;
    Node left = null, right = null;
 
    Node(int data) {
        this.data = data;
    }
}
 
class Main
{
    // Función para realizar un recorrido en orden en el árbol
    public static void inorder(Node root)
    {
        if (root == null) {
            return;
        }
 
        inorder(root.left);
        System.out.print(root.data + " ");
        inorder(root.right);
    }
 
    // Función para construir un árbol binario a partir de la array principal dada
    public static Node createTree(int[] parent)
    {
        // crea un mapa vacio
        Map<Integer, Node> map = new HashMap<>();
 
        // crear `n` nuevos nodos de árbol, cada uno con un valor de 0 a `n-1`,
        // y almacenarlos en un mapa
        for (int i = 0; i < parent.length; i++) {
            map.put(i, new Node(i));
        }
 
        // representa el nodo raíz de un árbol binario
        Node root = null;
 
        // recorrer la array principal y construir el árbol
        for (int i = 0; i < parent.length; i++)
        {
            // si el padre es -1, establece la raíz en el nodo actual que tiene el
            // valor `i` (almacenado en map[i])
            if (parent[i] == -1) {
                root = map.get(i);
            }
            else {
                // obtener el padre del nodo actual
                Node ptr = map.get(parent[i]);
 
                // si el hijo izquierdo del padre está lleno, asigna el nodo a su derecha
                // niño
                if (ptr.left != null) {
                    ptr.right = map.get(i);
                }
                // si el hijo izquierdo del padre está vacío, asigne el nodo a él
                else {
                    ptr.left = map.get(i);
                }
            }
        }
 
        // devuelve la raíz del árbol construido
        return root;
    }
 
    public static void main(String[] args)
    {
        int[] parent = {-1, 0, 0, 1, 2, 2, 4, 4};
 
        Node root = createTree(parent);
        inorder(root);
    }
}
```

Resultado:
```sh
3 1 0 6 4 7 2 5
```

La complejidad temporal de la solución anterior es O(n), dónde n es el número total de nodos en un árbol binario (suponiendo operaciones de tiempo constante para la tabla hash). El espacio auxiliar requerido por el programa es O(n).

