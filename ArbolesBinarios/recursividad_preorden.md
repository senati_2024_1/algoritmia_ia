# Recorrido de árbol de pedido anticipado: iterativo y recursivo

Dado un árbol binario, escriba una solución iterativa y recursivo para atravesar el árbol utilizando el recorrido de preorden en C++, Java y Python.

A diferencia de las listas enlazadas, los arrays unidimensionales y otras estructuras de datos lineales, que se recorren en orden lineal, los árboles se pueden recorrer de múltiples maneras en profundidad de primer orden (hacer un pedido, en orden, y orden de publicación) o ancho de primer orden (recorrido de orden de nivel). Más allá de estos recorridos básicos, son posibles varios esquemas más complejos o híbridos, como búsquedas limitadas en profundidad como la búsqueda iterativa de profundización primero en profundidad. En esta publicación, se analiza en detalle el recorrido del árbol de pedidos anticipados. 

 Atravesar un árbol implica iterar sobre todos los nodos de alguna manera. Como el árbol no es una estructura de datos lineal, puede haber más de un nodo siguiente posible a partir de un nodo dado, por lo que algunos nodos deben ser diferidos, es decir, almacenados de alguna manera para visitarlos más tarde. El recorrido se puede hacer iterativamente donde los nodos diferidos se almacenan en el stack, o se puede hacer por recursión, donde los nodos diferidos se almacenan implícitamente en el pila de llamadas.

Para atravesar un árbol binario (no vacío) en orden anticipado, debemos hacer estas tres cosas para cada nodo n a partir de la raíz del árbol:

(N) Proceso n sí mismo.
(L) Atraviesa recursivamentemente su subárbol izquierdo. Cuando termine este paso, estamos de vuelta en n otra vez.
(R) Atraviesa recursivamentemente su subárbol derecho. Cuando termine este paso, estamos de vuelta en n otra vez.

En el recorrido de preorden normal, visite el subárbol izquierdo antes que el subárbol derecho. Si visitamos el subárbol de la derecha antes de visitar el subárbol de la izquierda, se denomina recorrido de preorden inverso.

![5](/ArbolesBinarios/5.png)

## Implementación recursivo

Como podemos ver, solo después de procesar cualquier nodo, se procesa el subárbol izquierdo, seguido del subárbol derecho. Estas operaciones se pueden definir recursivamentemente para cada nodo. La implementación recursivamente se conoce como Búsqueda en profundidad (DFS), ya que el árbol de búsqueda se profundiza lo más posible en cada hijo antes de pasar al siguiente hermano.

El siguiente es el programa Java:

```java:
// Estructura de datos para almacenar un nodo de árbol binario
class Node
{
    int data;
    Node left, right;
 
    // Función para crear un nuevo nodo de árbol binario con una clave dada
    public Node(int key)
    {
        data = key;
        left = right = null;
    }
}
 
class Main
{
    // Función recursivo para realizar un recorrido de preorden en el árbol
    public static void preorder(Node root)
    {
        // regresa si el nodo actual está vacío
        if (root == null) {
            return;
        }
 
        // Mostrar la parte de datos de la raíz (o nodo actual)
        System.out.print(root.data + " ");
 
        // Recorrer el subárbol izquierdo
        preorder(root.left);
 
        // Recorrer el subárbol derecho
        preorder(root.right);
    }
 
    public static void main(String[] args)
    {
        /* Construimos el siguiente árbol
                   1
                 /   \
                /     \
               2       3
              /      /   \
             /      /     \
            4      5       6
                  / \
                 /   \
                7     8
        */
 
        Node root = new Node(1);
        root.left = new Node(2);
        root.right = new Node(3);
        root.left.left = new Node(4);
        root.right.left = new Node(5);
        root.right.right = new Node(6);
        root.right.left.left = new Node(7);
        root.right.left.right = new Node(8);
 
        preorder(root);
    }
}
```

## Implementación iterativa

Para convertir el procedimiento recursivo anterior en uno iterativo, necesitamos una stack explícita. El siguiente es un algoritmo iterativo simple basado en la stack para realizar un recorrido de preorden:

```sh:
iterativePreorder(node)

if (node = null)
    returns —> empty

s —> empty stacks
s.push(node)

while (not s.isEmpty()) 
    node —> s.pop()
    visit(node)
    
    if (node.right != null)
        s.push(node.right)

    if (node.left != null)
        s.push(node.left)
```
El algoritmo se puede implementar de la siguiente manera en Java:

```java:
import java.util.Stack;
 
// Estructura de datos para almacenar un nodo de árbol binario
class Node
{
    int data;
    Node left, right;
 
    // Función para crear un nuevo nodo de árbol binario con una clave dada
    public Node(int key)
    {
        data = key;
        left = right = null;
    }
}
 
class Main
{
    // Función iterativa para realizar un recorrido de preorden en el árbol
    public static void preorderIterative(Node root)
    {
        // regresa si el árbol está vacío
        if (root == null) {
            return;
        }
 
        // crea una stack vacía y empuja el nodo raíz
        Stack<Node> stack = new Stack<>();
        stack.push(root);
 
        // bucle hasta que la stack esté vacía
        while (!stack.empty())
        {
            // sacar un nodo de la stack e imprimirlo
            Node curr = stack.pop();
 
            System.out.print(curr.data + " ");
 
            // inserta el hijo derecho del nodo emergente en la stack
            if (curr.right != null) {
                stack.push(curr.right);
            }
 
            // empuja el hijo izquierdo del nodo extraído a la stack
            if (curr.left != null) {
                stack.push(curr.left);
            }
 
            // el hijo derecho debe ser empujado primero para que el hijo izquierdo
            // se procesa primero (orden LIFO)
        }
    }
 
    public static void main(String[] args)
    {
        /* Construimos el siguiente árbol
                   1
                 /   \
                /     \
               2       3
              /      /   \
             /      /     \
            4      5       6
                  / \
                 /   \
                7     8
        */
 
        Node root = new Node(1);
        root.left = new Node(2);
        root.right = new Node(3);
        root.left.left = new Node(4);
        root.right.left = new Node(5);
        root.right.right = new Node(6);
        root.right.left.left = new Node(7);
        root.right.left.right = new Node(8);
 
        preorderIterative(root);
    }
}
```

La solución anterior se puede optimizar aún más empujando solo a los niños correctos a la stack.

```java:
import java.util.Stack;
 
// Estructura de datos para almacenar un nodo de árbol binario
class Node
{
    int data;
    Node left, right;
 
    // Función para crear un nuevo nodo de árbol binario con una clave dada
    public Node(int key)
    {
        data = key;
        left = right = null;
    }
}
 
class Main
{
    // Función iterativa para realizar un recorrido de preorden en el árbol
    public static void preorderIterative(Node root)
    {
        // regresa si el árbol está vacío
        if (root == null) {
            return;
        }
 
        // crea una stack vacía y empuja el nodo raíz
        Stack<Node> stack = new Stack<>();
        stack.push(root);
 
        // comienza desde el nodo raíz (establece el nodo actual en el nodo raíz)
        Node curr = root;
 
        // bucle hasta que la stack esté vacía
        while (!stack.empty())
        {
            // si el nodo actual existe, imprímalo y empuje su hijo derecho
            // a la stack antes de pasar a su hijo izquierdo
            if (curr != null)
            {
                System.out.print(curr.data + " ");
 
                if (curr.right != null) {
                    stack.push(curr.right);
                }
 
                curr = curr.left;
            }
            // si el nodo actual es nulo, saca un nodo de la stack
            // establecer el nodo actual en el nodo reventado
            else {
                curr = stack.pop();
            }
        }
    }
 
    public static void main(String[] args)
    {
        /* Construimos el siguiente árbol
                   1
                 /   \
                /     \
               2       3
              /      /   \
             /      /     \
            4      5       6
                  / \
                 /   \
                7     8
        */
 
        Node root = new Node(1);
        root.left = new Node(2);
        root.right = new Node(3);
        root.left.left = new Node(4);
        root.right.left = new Node(5);
        root.right.right = new Node(6);
        root.right.left.left = new Node(7);
        root.right.left.right = new Node(8);
 
        preorderIterative(root);
    }
}
```

La complejidad temporal de las soluciones anteriores es O(n), dónde n es el número total de nodos en el árbol binario. La complejidad espacial del programa es O(n) ya que el espacio requerido es proporcional a la altura del árbol, que puede ser igual al número total de nodos en el árbol en el peor de los casos para árboles sesgados.

