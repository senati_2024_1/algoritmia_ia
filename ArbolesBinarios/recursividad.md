# Recorrido de árbol en orden: iterativo y recursivo

A diferencia de las listas enlazadas, los arrays unidimensionales y otras estructuras de datos lineales, que se recorren en orden lineal, los árboles se pueden recorrer de múltiples maneras en profundidad de primer orden (hacer un pedido, en orden, y orden de publicación) o ancho de primer orden (recorrido de orden de nivel). Más allá de estos recorridos básicos, son posibles varios esquemas más complejos o híbridos, como búsquedas limitadas en profundidad como la búsqueda iterativa de profundización primero en profundidad. En esta publicación, se analiza en detalle el recorrido del árbol en orden. 

 Atravesar un árbol implica iterar sobre todos los nodos de alguna manera. Como el árbol no es una estructura de datos lineal, puede haber más de un nodo siguiente posible a partir de un nodo dado, por lo que algunos nodos deben ser diferidos, es decir, almacenados de alguna manera para visitarlos más tarde. El recorrido se puede hacer iterativamente donde los nodos diferidos se almacenan en el stack, o se puede hacer por recursión, donde los nodos diferidos se almacenan implícitamente en el pila de llamadas.

Para atravesar un árbol binario (no vacío) en orden, debemos hacer estas tres cosas para cada nodo n a partir de la raíz del árbol:

(L) Atraviesa recursivamentemente su subárbol izquierdo. Cuando termine este paso, estamos de vuelta en n otra vez.
(N) Proceso n sí mismo.
(R) Atraviesa recursivamentemente su subárbol derecho. Cuando termine este paso, estamos de vuelta en n otra vez.

En un recorrido en orden normal, visitamos el subárbol izquierdo antes que el subárbol derecho. Si visitamos el subárbol derecho antes de visitar el subárbol izquierdo, se denomina recorrido en orden inverso.

![1](/ArbolesBinarios/1.png)

## Implementación recursivo

Como podemos ver, antes de procesar cualquier nodo, primero se procesa el subárbol izquierdo, seguido del nodo y, por último, el subárbol derecho. Estas operaciones se pueden definir recursivamentemente para cada nodo. La implementación recursivamente se conoce como Búsqueda en profundidad (DFS), ya que el árbol de búsqueda se profundiza lo más posible en cada hijo antes de pasar al siguiente hermano.

El siguiente es el programa Java que lo demuestra:

```java:
// Estructura de datos para almacenar un nodo de árbol binario
class Node
{
    int data;
    Node left, right;
 
    // Función para crear un nuevo nodo de árbol binario con una clave dada
    public Node(int key)
    {
        data = key;
        left = right = null;
    }
}
 
class Main
{
    // Función recursivo para realizar un recorrido en orden en el árbol
    public static void inorder(Node root)
    {
        // regresa si el nodo actual está vacío
        if (root == null) {
            return;
        }
    
        // Recorrer el subárbol izquierdo
        inorder(root.left);
    
        // Mostrar la parte de datos de la raíz (o nodo actual)
        System.out.print(root.data + " ");
    
        // Recorrer el subárbol derecho
        inorder(root.right);
    }
 
    public static void main(String[] args)
    {
        /* Construimos el siguiente árbol
                   1
                 /   \
                /     \
               2       3
              /      /   \
             /      /     \
            4      5       6
                  / \
                 /   \
                7     8
        */
 
        Node root = new Node(1);
        root.left = new Node(2);
        root.right = new Node(3);
        root.left.left = new Node(4);
        root.right.left = new Node(5);
        root.right.right = new Node(6);
        root.right.left.left = new Node(7);
        root.right.left.right = new Node(8);
 
        inorder(root);
    }
}
```

Implementación iterativa
Para convertir el procedimiento recursivo anterior en uno iterativo, necesitamos una stack explícita. El siguiente es un algoritmo iterativo simple basado en la stack para realizar un recorrido en orden:

```sh
iterativeInorder(node)
s —> empty stack
while (not s.isEmpty() or node != null)
    if (node != null)
        s.push(node)
        node —> node.left
    else      node —> s.pop()
        visit(node)
        node —> node.right
```

El algoritmo se puede implementar de la siguiente manera:

```java:
import java.util.Stack;
 
// Estructura de datos para almacenar un nodo de árbol binario
class Node
{
    int data;
    Node left, right;
 
    // Función para crear un nuevo nodo de árbol binario con una clave dada
    public Node(int key)
    {
        data = key;
        left = right = null;
    }
}
 
class Main
{
    // Función iterativa para realizar un recorrido en orden en el árbol
    public static void inorderIterative(Node root)
    {
        // crea una stack vacía
        Stack<Node> stack = new Stack<>();
    
        // comienza desde el nodo raíz (establece el nodo actual en el nodo raíz)
        Node curr = root;
    
        // si el nodo actual es nulo y la stack también está vacía, hemos terminado
        while (!stack.empty() || curr != null)
        {
            // si el nodo actual existe, empujarlo a la stack (diferirlo)
            // y pasar a su hijo izquierdo
            if (curr != null)
            {
                stack.push(curr);
                curr = curr.left;
            }
            else {
                // de lo contrario, si el nodo actual es nulo, extraiga un elemento de
                // la stack, la imprime y finalmente establece el nodo actual en su
                // hijo derecho
                curr = stack.pop();
                System.out.print(curr.data + " ");
    
                curr = curr.right;
            }
        }
    }
 
    public static void main(String[] args)
    {
        /* Construimos el siguiente árbol
                   1
                 /   \
                /     \
               2       3
              /      /   \
             /      /     \
            4      5       6
                  / \
                 /   \
                7     8
        */
 
        Node root = new Node(1);
        root.left = new Node(2);
        root.right = new Node(3);
        root.left.left = new Node(4);
        root.right.left = new Node(5);
        root.right.right = new Node(6);
        root.right.left.left = new Node(7);
        root.right.left.right = new Node(8);
 
        inorderIterative(root);
    }
}
```

La complejidad temporal de las soluciones anteriores es O(n), dónde n es el número total de nodos en el árbol binario. La complejidad espacial del programa es O(n) ya que el espacio requerido es proporcional a la altura del árbol, que puede ser igual al número total de nodos en el árbol en el peor de los casos para árboles sesgados.

