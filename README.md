# Repositorio - Sesiones de aprendizaje 202410
## Curso: Algoritmia para la inteligencia artificial
## Instructor: Ing. Franklin Condori

## Libros de consulta

* [Inteligencia Artificial para desarrolladores, 2da Edición - Virginie Mathivet](https://drive.google.com/file/d/1I0sJAl7ngZmvNoKP5dsYKX0K0S4Oik1G/view?usp=sharing)
* [Ejemplos del libro](https://drive.google.com/file/d/1Tq72rqeZEdbfOZcI87qMfFYmG2fRehkC/view?usp=sharing)
* [Matematicas discretas](https://webooks.co/images/team/academicos/matematicas/matematicas/2.Matematicas%20Discretas%20-%203edi%20Lipschutz.pdf)

## Manuales

* [Control de versiones con Git](https://drive.google.com/file/d/1ZJhtCVPW-lYtvuvvXoa3sf8ih5Exw_1I/view?usp=sharing)
* [Curso profesional git y github - Platzi](https://drive.google.com/file/d/1ZRCsphOYKlTJwNJHCMUFZyWy6QuQhn7N/view?usp=sharing)
* [Conociendo Weka](https://drive.google.com/file/d/1Zg_uClgQ0V95kBhEMuTFzdqVwLXQQc7H/view?usp=sharing)
* [Arboles binarios](https://www.oscarblancarteblog.com/2014/08/22/estructura-de-datos-arboles/)

## Presentaciones - Sesiones aprendizaje

* [1. Introducción](https://docs.google.com/presentation/d/10aX_Pg1nZZseJ0EyxhZ9pcKSS9eTZVmk/edit?usp=sharing&ouid=115384914012814073245&rtpof=true&sd=true)
* [2. Machine Learning](https://docs.google.com/presentation/d/1L_C7rFHAkfqUjesfUcoUI9GQDOrfDJky/edit?usp=sharing&ouid=115384914012814073245&rtpof=true&sd=true)
* [3. Metodología CRISP-DM](https://docs.google.com/presentation/d/1uC2rpvvm6c6_zi3XNaqeah6Dh2xdu9pJ/edit?usp=sharing&ouid=115384914012814073245&rtpof=true&sd=true)
* [4. Arbol binario I](https://docs.google.com/presentation/d/1NiIETfmasFBMlQP1LEAlBAhDd7QSgeVm/edit?usp=sharing&ouid=115384914012814073245&rtpof=true&sd=true)
* [5. Arbol binario II](https://docs.google.com/presentation/d/1-SnHkvYQA_tYmLQOMWQdILhISshC5ZbP/edit?usp=sharing&ouid=115384914012814073245&rtpof=true&sd=true)
* [6. Arboles binarios III Plataforma externa](https://drive.google.com/file/d/1QhYpa6_03MMqHd2P1a-51-394J72JHtY/view?usp=sharing)
* [7. Algoritmo ID3](AlgoritmoID3/ID3.md)

## Materiales audiovisuales

*  [¿La inteligencia artificial amenaza la identidad humana?](https://www.youtube.com/watch?v=YUZoeRTRFyU)
*  [Las aplicaciones belicas de la inteligencia artificial en la actualidad](https://www.youtube.com/watch?v=i7FVP6NaVo4)

### Trabajos

* [1. Conocimientos previos](trabajos/conocimientos_previos.md)
* [2. Codificar y ejecutar los algoritmos del manual de estructura de datos](https://drive.google.com/file/d/1zYbPCdnbCWTqPR5EjALokUpbjg9e5ol6/view?usp=sharing)
* [3. Resolver los recorridos de los arboles binarios](https://drive.google.com/file/d/1TvvqsaUyNn0mZ-SftgBuKbxNTSlY07LO/view?usp=sharing)
* [4. Codificar y ejecutar el ejemplo de arbol binario en Java](trabajos/ArbolBinario.md)
* [5. Resolver los ejercicios](https://drive.google.com/file/d/1kMywgwDEnwBt4fp8Qyzm9HV0djlwwjod/view?usp=sharing)

### Practicas de clase
* [Practica 1: Aprendiendo a usar Weka](https://isa.umh.es/asignaturas/iarp/practicas/P_1/PRACTICA1.pdf)
* [Practica 2: Reforzamiento](https://drive.google.com/file/d/18zU0EALacb8o1_ggpDNb0kMRiqqgkExc/view?usp=sharing)
* [Practica 3: Arbol binario básico](trabajos/Ejemplos/Nodo.md)
* [Practica 4: Arboles binarios avanzados](https://drive.google.com/drive/folders/1TqaPnNjdurUWunQFhDKTXZCFSoi8Q9UN?usp=sharing)
* [Practica 5: Arboles Binarios - Recursividad inorder](ArbolesBinarios/recursividad.md)
* [Practica 6: Arboles Binarios - Recursividad postorder](ArbolesBinarios/recursividad_postorden.md)
* [Practica 7: Arboles Binarios - Recursividad preorder](ArbolesBinarios/recursividad_preorden.md)
* [Practica 8: Arboles Binarios - Crear un arbol a partir de un arreglo](ArbolesBinarios/crear_arbol_array.md)
* [Practica 9: Arboles Binarios - Imprimir estructura](ArbolesBinarios/Imprimir_estructura.md)
* [Practica 10: Construyendo un arbol de decisión](https://senatipe-my.sharepoint.com/:x:/g/personal/rcondoriq_senati_pe/Ea7pCrpTkvhEtSG_vTdT-IEB-17KmOHSW1VCIvKi--WKcg?e=47iqDI)
