# Algoritmo ID3 del árbol de decisión

## 1. Algoritmo ID3 del árbol de decisión

Un árbol de decisiones es un árbol construido en base a decisiones. En el aprendizaje automático, un árbol de decisión es un modelo predictivo que representa una relación de mapeo entre los atributos del objeto y los valores del objeto. Cada nodo representa un objeto y cada ruta de rama en el árbol representa una posibilidad.

Los nodos hoja, a su vez, reflejan los valores finales del objeto correspondiente a la secuencia de decisiones tomadas desde el nodo raíz hasta ese punto. Los árboles de decisión suelen tener una única salida. En casos donde existen múltiples salidas, se pueden generar árboles de decisión independientes para abordar cada una de estas salidas de manera individual. A continuación, profundizaremos en el algoritmo 1D3.

## 2. Introducción al algoritmo ID3

*El algoritmo ID3*, conocido como **Iterative Dichotomiser 3**(*árbol binario iterativo de tres generaciones*), es un tipo específico de árbol de decisión. Este se fundamenta en *el principio de la navaja de Occam*, el cual aboga por utilizar la menor cantidad de elementos posible para lograr resultados efectivos. Fue desarrollado por Ross Quinlan.

La esencia del algoritmo radica en la aplicación de este principio: se busca crear árboles de decisión lo más compactos posible, aunque esto no siempre sea factible.

Se trata de un algoritmo heurístico que prioriza la generación de la estructura de árbol más reducida. En la teoría de la información, se entiende que a menor información esperada, mayor será la ganancia de información y, por ende, la pureza de los resultados. Por tanto, el objetivo central del algoritmo ID3 es minimizar la información necesaria para tomar decisiones precisas.

Para ello, se evalúa la ganancia de información de cada atributo y se selecciona aquel que brinde la mayor ganancia tras la división. Este proceso se realiza mediante un enfoque de búsqueda codiciosa, de arriba hacia abajo, explorando los posibles espacios de decisión de manera sistemática.

## 3. Entropía de información y ganancia de información

En el proceso de obtener información, es crucial entender cuánta información puede contribuir una característica al sistema de clasificación. Cuanta más información pueda ofrecer, más relevante será dicha característica.

Antes de abordar la ganancia de información, es esencial entender qué es la entropía de información. El concepto de *entropía* tiene su origen en la física, donde se utiliza para medir el grado de desorden de un sistema termodinámico. Sin embargo, en el ámbito de las ciencias de la información, la entropía representa una medida de incertidumbre. En 1948, Shannon introdujo la entropía de la información, definiéndola como la probabilidad de ocurrencia de eventos aleatorios discretos.

En términos simples, cuanto más organizado esté un sistema, menor será su entropía de información, mientras que un sistema más caótico tendrá una entropía de información mayor. Así pues, la entropía de información puede considerarse como el grado de orden en un sistema, una medida de la incertidumbre.

La entropía de información se calcula considerando la probabilidad de ocurrencia de cada categoría en el sistema de clasificación. Para el sistema de clasificación, donde cada categoría es una variable y su valor es la probabilidad de ocurrencia de dicha categoría, la entropía se expresa como:

$`Entropia H(S)=-\sum_{i=1}^{n}p(i)\log_{2}p(i)`$

Donde $`p(i)`$ es la probabilidad de la i-ésima categoría y $`n`$ es el número total de categorías.


Una vez entendida la entropía de información, podemos adentrarnos en el concepto de ganancia de información. La ganancia de información para una característica específica se refiere a la diferencia en la cantidad de información presente en el sistema con y sin esa característica. En otras palabras, es la medida de cuánta información adicional aporta dicha característica al sistema.

## 4. Ganancia de la información

La ganancia de información indica cuánta información nos brinda una característica particular o una variable particular sobre los resultados finales. Se puede medir utilizando la fórmula

$`\text{Ganancia}(A,S) = H(S)-\sum_{j=1}^{v}\frac{|S_j|}{|S|}. H(S_j) = H(S)-H(A,S)`$

Donde $`H(S)`$ es la entropía del conjunto $`S`$, $`|S_j|`$ es el número de instancia $`j`$ de un atributo $`A`$, $`|S|`$ es el número total de instancias de un conjunto $`S`$, $`v`$ es el conjunto de valores distintos de un atributo $`A`$, $`H(S_j)`$ es la entropía del subconjunto de instancias para el atributo $`A`$ y $`H(A,S)`$ es la entropía de un atributo $`A`$.

## 5. Ejemplo

Supongamos que deseamos predecir si se jugará o no al aire libre, basándonos en ciertos datos meteorológicos. Las variables predictoras aquí son el pronóstico, la humedad y el viento.

El valor de la variable objetivo es que se pueda o no jugar el juego. La predicción «No» significa que las condiciones climáticas no son buenas y por lo tanto, no se puede jugar. La predicción «Sí» significa que se puede jugar, por lo que la jugada tiene un valor que es «sí» o «no».

Los datos datos del ejercicio son los siguientes:

![1](/AlgoritmoID3/id1.png)

Ahora, para resolver tal problema, hacemos uso de un árbol de decisión.

![2](/AlgoritmoID3/id2.png)

Consideremos un árbol donde cada rama del árbol denota alguna decisión. Cada rama se conoce como un nodo de rama, y ​​en cada rama, debemos decidir de tal manera que pueda obtener un resultado al final de la rama. En la siguiente imagen muestra que de las 14 observaciones, 9 observaciones dan como resultado un sí, lo que significa que de los 14 días, el partido se puede jugar solo en 9 días. Así que aquí si ven los días 1, 2, 8, 9 y 11, el panorama ha sido soleado. Básicamente, estamos tratando de agrupar conjuntos de datos según la perspectiva. Cuando hace sol, tenemos dos “Sí” y tres “No”; cuando la perspectiva está nublada, tenemos los cuatro como Sí, lo que significa que en los 4 días en que la perspectiva estaba nublada, podemos jugar el juego.

Cuando se trata de lluvia, tenemos tres «Sí» y dos «No». La decisión se puede tomar según la variable de perspectiva en el nodo raíz. Entonces, el nodo raíz es el nodo superior en un árbol de decisión. Ahora, lo que hemos hecho aquí es que hemos creado un árbol de decisiones que comienza con el nodo pronóstico y luego lo dividimos aún más según otros parámetros, como soleado, nublado y lluvioso.

Lo que estamos haciendo es tomar el árbol de decisiones eligiendo la variable de perspectiva en el nodo raíz. El nodo raíz es el nodo superior en un árbol de decisión. El nodo Pronóstico tiene tres ramas que salen: soleado, nublado y lluvioso. Estos tres valores se asignan al nodo de la rama intermedia y se calculan para la posibilidad de jugar igual a «sí». Para ramas soleadas y lluviosas, si es una mezcla de sí y no, dará una salida impura (entrópicamente hablando). Pero cuando se trata de la variable nublada, da como resultado un salida 100% pura (entrópicamente hablando). Esto muestra que la variable nublada dará como resultado una salida definida y cierta. Esto es exactamente lo que se usa para medir la entropía que calcula la impureza o la incertidumbre. Entonces, cuanto menor es la incertidumbre o la entropía de una variable, más significativa es esa variable.

Cuando se trata de días nublados que no tienen impurezas en un conjunto de datos, es un subconjunto puro perfecto. No siempre tenemos suerte y no siempre encontramos variables que den como resultado un subconjunto para medir la entropía. Por lo tanto, cuanto menor sea la entropía de una variable particular, más significativa será esa variable. En el árbol de decisión, el atributo asignado del nodo raíz se considera para el resultado preciso. Esto significa que el nodo raíz debe tener la variable más significativa, razón por la cual elegimos el pronóstico.

El nodo nublado no es una variable sino el subconjunto del nodo raíz pronóstico. Ahora la pregunta es ¿Cómo decidir qué variable o atributo divide mejor los datos?. Cuando se trata de árboles de decisión, ganancia de información y entropía, nos ayudará a comprender qué variable dividirá mejor el conjunto de datos o qué variable se usa para asignar al nodo raíz. Las variables asignadas al nodo raíz se dividirán según el conjunto de datos con las variables más significativas.

De forma resumida podemos clasificar nuestros nodos de la siguiente manera:

![3](/AlgoritmoID3/id3.png)

### Cálculo de la ganancia de la información

De acuerdo a la fórmulas anteriores, podemos encontrar la ganancia de información y la entropía. Del total de 14 instancias que vimos, 9 dijeron que sí y 5 dijeron que no, lo que significa que no puedes jugar ese día en particular. Entonces podemos calcular la entropía usando el árbol de decisión anterior para la predicción. Susutituimos los valores en la fórmula:

![4](/AlgoritmoID3/id4.png)

$`H(s)=-\frac{9}{14}\log_2\frac{9}{14}-\frac{5}{14}\log_2\frac{5}{14}=0.94`$

Calculando la entropia del atributo general, mediante el siguiente procedimiento:

![5](/AlgoritmoID3/id5.png)


Cuando sustituye los valores en la fórmula, obtiene un valor de 0.94. Esta es la entropía y esta es la incertidumbre de los datos presentes en la muestra. Ahora, para asegurarnos de elegir la mejor variable para el nodo raíz, veamos las posibles combinaciones que puede usar en el nodo raíz.

Todas las combinaciones posibles que puede usar en el nodo raíz se muestran en la siguiente figura. La combinación posible puede ser pronóstico, viento, humedad o temperatura. Estas son cuatro variables y puede tener cualquiera de estas variables como su nodo raíz. Pero ¿Cómo selecciona la variable que mejor se ajusta al nodo raíz? Aquí, podemos usar la ganancia de información y la entropía. Por lo tanto, la tarea es encontrar la ganancia de información para cada uno de estos atributos, es decir, para perspectiva, viento, humedad y temperatura. Debe elegirse la variable que resulte en la mayor ganancia de información porque proporciona la información de salida más precisa.

### Calculando las entropias para los atributos

#### Atributo Temperatura

![6](/AlgoritmoID3/id6.png)

#### Atributo Humedad

![7](/AlgoritmoID3/id7.png)

#### Atributo Viento

![8](/AlgoritmoID3/id8.png)

### Contruyendo el arbol

#### Nodo Soleado

![9](/AlgoritmoID3/id9.png)

![10](/AlgoritmoID3/id10.png)

![11](/AlgoritmoID3/id11.png)

#### El Atributo con mayor ganacia para el primer arbol es Humedad por tener una mayor ganacia

![12](/AlgoritmoID3/id12.png)

![13](/AlgoritmoID3/id13.png)

![14](/AlgoritmoID3/id14.png)

![15](/AlgoritmoID3/id15.png)

#### Nodo Nublado

![16](/AlgoritmoID3/id16.png)

![17](/AlgoritmoID3/id17.png)

#### Nodo Lluvioso

![18](/AlgoritmoID3/id18.png)

![19](/AlgoritmoID3/id19.png)

![20](/AlgoritmoID3/id20.png)

![21](/AlgoritmoID3/id21.png)

![22](/AlgoritmoID3/id22.png)

![23](/AlgoritmoID3/id23.png)